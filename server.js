var express  = require('express'),
	compress = require('compression'),
	config   = require('./config'),
	app      = express();

if(config.environment === 'production') {
	app.use(compress());
}
app.use(express.static(__dirname + config.assets.folder));
app.set('view engine', 'ejs');

app.get('/', function (request, response) {
	response.render(__dirname + config.public.views.homepage);
});

app.get('/quick-start', function (request, response) {
	response.render(__dirname + config.public.views.quickstart);
});

var server = app.listen(config.server.port, config.server.address, function () {
	var host = server.address().address;
	var port = server.address().port;

	console.log('nw.js is listening at ' + config.server.protocol + '://' + host + ':' + port);
});