var gulp     = require('gulp'),
	less     = require('gulp-less')
	rename   = require('gulp-rename'),
	minify   = require('gulp-minify-css'),
	uglify   = require('gulp-uglify'),
	concat   = require('gulp-concat'),
	del      = require('del'),
	promise  = require('q'),
	config   = require('./config'),
	clean = function() {
		var deferred = promise.defer();
		
		try {
			del('assets/');
			deferred.resolve();
		}
		catch(error) {
			console.log(error);
			deferred.reject();
		}

		return deferred.promise;
	};


gulp.task('fonts', function() {
	return gulp.src(config.assets.input.fonts)
		.pipe(gulp.dest(config.assets.output.fonts));
});


gulp.task('styles', function() {
	return gulp.src(config.assets.input.css)
		.pipe(less())
		.pipe(gulp.dest(config.assets.output.css))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(minify({
			compatibility: 'ie8',
			keepSpecialComments: 0
		}))
		.pipe(gulp.dest(config.assets.output.css));
});

gulp.task('scripts', function() {
	return gulp.src(config.assets.input.js)
	    .pipe(concat('custom.js'))
	    .pipe(gulp.dest(config.assets.output.js))
	    .pipe(uglify())
	    .pipe(rename({
	    	suffix: '.min'
	    }))
	    .pipe(gulp.dest(config.assets.output.js));
});


gulp.task('images', function() {
	return gulp.src(config.assets.input.images)
		.pipe(gulp.dest(config.assets.output.images));
});


gulp.task('build', function() {
	clean().done(function() {
		gulp.start('fonts', 'styles', 'scripts');
	});
});


gulp.task('watch', function() {
	gulp.watch('src/js/**', ['scripts']);
	gulp.watch('src/css/**', ['styles']);
});