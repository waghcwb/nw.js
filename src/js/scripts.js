/*globals console */
(function () {
	
	'use strict';

	function NW(scope) {
		if (!(this instanceof NW)) {
			return new NW();
		} else {
			console.warn('already defined');
		}

		this.githublink = 'https://api.github.com/repos/nwjs/nw.js/contributors?access_token=';
		this.token = 'e069bde6aa5e841e7ee1e232adf9900d99c27515';
		this.scope = scope;

		return this;
	}

	NW.fn = NW.prototype = {};

	NW.fn.ready = function (callback) {
		if (document.readyState !== 'loading') {
			callback();
		} else {
			document.addEventListener('DOMContentLoaded', callback);
		}
	};

	NW.fn.ajax = {
		get: function (url, callback) {
			var request = new XMLHttpRequest();

			request.open('GET', url, true);

			request.onload = function () {
				if (this.status >= 200 && this.status < 400) {
					if (callback) {
						callback(this.response);
					}
				} else {
					console.error('something went wrong.', this.status, this.response);
				}
			};

			request.onerror = function (error) {
				console.info(error);
			};

			request.send();
		}
	};

	var nw = NW();

	NW.fn.github = {
		getUsers: function (callback) {
			nw.ajax.get(nw.githublink + nw.token, function (data) {
				callback(data);
			});
		}
	};

	NW.fn.init = function () {
		nw.github.getUsers(function (data) {
			var users = JSON.parse(data),
                i,
                mate,
                team = document.querySelector('.team');

            if(!team) {
            	return;
            }

            team.innerHTML = '';

			for (i in users) {
                if (users.hasOwnProperty(i)) {

                    mate = document.createElement('div');

                    mate.classList.add('mate', 'clearfix');
                    mate.innerHTML = '<img src="' + users[i].avatar_url + '" alt="' + users[i].login + '" class="avatar" title="' + users[i].login + '">';
                    mate.innerHTML += '<h5 class="username"><a href="' + users[i].html_url + '" target="blank">' + users[i].login + '</a></h5>';
                    mate.innerHTML += '<span class="contributions" title="Contributions">' + users[i].contributions + '</span>';

                    team.appendChild(mate);
                }
			}
		});

		if(document.querySelector('pre code')) {
			hljs.highlightBlock(document.querySelector('pre code'));
		}
	};

	nw.ready(function () {
		nw.init();
	});

}());